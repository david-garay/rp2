import csv
import pandas as pd
import numpy as np
import time
import pprint
import re
from os import listdir
from datetime import datetime
import matplotlib.pyplot as plt
import pylab
import math
import seaborn as sns
import warnings
from scipy import stats
plt.rcParams['font.size'] = 10.0


# https://medium.com/jbennetcodes/dealing-with-datetimes-like-a-pro-in-pandas-b80d3d808a7f
# boxplot: https://towardsdatascience.com/ways-to-detect-and-remove-the-outliers-404d16608dba
# Nice one on outliers: https://www.theanalysisfactor.com/outliers-to-drop-or-not-to-drop/
# trends, seasonability: https://www.datacamp.com/community/tutorials/time-series-analysis-tutorial

def process(autnum_sourceFile, route_sourceFile, policy_sourceFile):
    start_time = time.time()
    df_total,df_total_policy,df_autnum,df_route = load_to_dataframes(autnum_sourceFile,route_sourceFile,policy_sourceFile)
    
    # draw_histogram_count_changes(df_total,start='2019-01',end='2019-06')

 # BEGIN DRAWING
    # draw_boxplot_source(df_total) # Status: done, graph does not convey much info?
    # draw_boxplot_source(df_total,no_outliers=False) # Status: done, graph does not convey much info?

    # ### 1. Min/Max/Avg behavior
    # ### 1.1- Timeseries - Overview
    draw_ts_period(df_total,start='2004-02',end='2019-06') # Total
    # draw_ts_period(df_total,False,start='2004-02',end='2019-01') # Total
    # draw_ts_period(df_total,True,start='2018-01',end='2019-06') # Last 1.5 Year
    # draw_ts_period(df_total,False,start='2018-01',end='2019-06') # Last 1.5 Year
    draw_ts_period(df_total,True,start='2019-01',end='2019-06') # Last 6 months with averages per day and month
    # draw_ts_period(df_total,False,start='2019-01',end='2019-06') # Last 6 months with averages per day and month
    # draw_ts_period(df_total,True,start='2019-04',end='2019-05') # April, zoom 27
    # draw_ts_period(df_total,False,start='2019-04',end='2019-05') #April, zoom 27
    ## TRENDS: extract outliers?
    # draw_ts_rolling(df_total) # Moving average over 12 month
    # draw_ts_rolling(df_total,False) # Moving average over 12 month
    # draw_ts_diff(df_total) # Moving average over 12 month
    # draw_ts_diff(df_total,False) # Moving average over 12 month
    # draw_ts_regplot(df_total) # BROKEN: linear regression
    # draw_ts_regplot(df_total,False) # BROKEN: linear regression
    
    # 2. Operator behavior - Note: I need here asn information!!! df_autnum and route6 dus!!!!
    # 2.1- Bar chart nbr changes vs groups (like histogram)
    # draw_bar_count_changes(df_autnum) #Note, add route objects - Note: do last 18 months
    # 2.2- Pie chart nbr changes vs groups (like histogram)
    draw_pie_count_changes(df_autnum,start='2019-01',end='2019-06')
    ## 2.3- Historam chart nbr changes vs groups (like histogram)
    draw_histogram_count_changes(df_total,start='2019-01',end='2019-06')
     #Notes: identify when trend starts, then: from then on, update the pie chart.
     #Notes: add notable dates/milestones: add secure route servers.

    ### 3. Other
    ### 3.1- Bar chart source discrepancy
    # draw_bar_source(df_total_policy,df_total)
    ### 3.2- Box chart - How is the data distributed? box shows middle portion of data, dots represent outliers.
    draw_boxplot_source(df_total,start='2009-01',end='2019-06') # Status: done, graph does not convey much info?
    # draw_boxplot_source(df_total,no_outliers=False) # Status: done, graph does not convey much info?

##############  ACTION: FREQUENCY TABLE
    # df_route_count.plot(marker='.', alpha=0.5, linestyle='None', figsize=(11, 9), subplots=True)
    # pylab.show()
# PRINT SUMMARIES, STATS   
    print("PROCESSED in  %s seconds ---" %((time.time() - start_time)))
# DIFFERENT output formats


def load_to_dataframes(autnum_sourceFile,route_sourceFile,policy_sourceFile):
    try:
        df_autnum=pd.read_csv(autnum_sourceFile, names=["asn","timestamp"])
        df_route=pd.read_csv(route_sourceFile, names=["asn","prefix","timestamp"])
        df_route_policy=pd.read_csv(policy_sourceFile, names=["asn","prefix","timestamp"])
    # # read timestamps as time, granularity hour
        # PARSE TIMESTAMP & ROUND DOWN & WRITE TO FILE
        df_autnum['timestamp'] = pd.to_datetime(df_autnum['timestamp'], utc=True)
        df_autnum.set_index('timestamp', inplace=True)
        df_autnum.index = df_autnum.index.floor('1H')
        df_autnum_count = df_autnum.groupby(['timestamp']).count()
        # PARSE TIMESTAMP & ROUND DOWN & WRITE TO FILE
        df_route['timestamp'] = pd.to_datetime(df_route['timestamp'], utc=True)
        df_route.set_index('timestamp', inplace=True)
        df_route.index = df_route.index.floor('1H')
        df_route_count = df_route.groupby(['timestamp']).count()
        # ADD AGGREGATED DATA
        df_total = pd.merge(df_route_count, df_autnum_count, left_on='timestamp',right_on='timestamp', how='outer')
        df_total['aggregated_result'] = df_total[['asn_x','asn_y']].max(axis=1)
        # SAVE to CSV
        df_total.to_csv(path_or_buf ="./total.out",index=True)
 # Adding Policy files
        df_route_policy['timestamp'] = pd.to_datetime(df_route_policy['timestamp'], utc=True)
        df_route_policy.set_index('timestamp', inplace=True)
        df_route_policy.index = df_route_policy.index.floor('1H')
        df_route_policy_count = df_route_policy.groupby(['timestamp']).count()
        
        df_total_policy = pd.merge(df_route_policy_count, df_autnum_count, left_on='timestamp',right_on='timestamp', how='outer')
        # print(df_total)

        # df_total_policy['aggregated_result'] = df_total_policy[['asn_x','prefix','asn_y']].apply(\
        #     lambda x:
        #         (x['asn_y'] if math.isnan(x['asn_x']) else x['asn_x']) if (math.isnan(x['asn_x']) or math.isnan(x['asn_y']))\
        #              else x['asn_y'],axis=1)
        df_total_policy['aggregated_result'] = df_total_policy[['asn_x','asn_y']].max(axis=1)
        df_total_policy.to_csv(path_or_buf ="./total_policy.out",index=True)
    except IOError:
        print("Error opening ", autnum_sourceFile, route_sourceFile)
    return df_total, df_total_policy,df_autnum,df_route

 # end
def draw_bar_source(src1,src2,start='2019-03',end='2019-05'):
    sns.set_style("darkgrid")
    tmp=src1.loc[start:end].resample('M').mean().reset_index()
    tmp["source"] = pd.Series("policy",index=tmp.index)
    tmp2=src2.loc[start:end].resample('M').mean().reset_index()
    tmp2["source"] = pd.Series("historical data",index=tmp2.index)

    concatenated = pd.concat([tmp,tmp2])
    print(concatenated)
    # fig, ax = plt.subplots(1, 2)
    ax = sns.barplot(x="timestamp", y="aggregated_result",hue="source", data=concatenated)
    show_values_on_bars(ax)
    ax.set_xticklabels(ax.get_xticklabels())#,rotation=30)
    plt.show()

# def draw_bar_count_changes(src1,start='2001-01',end='2019-06'):
#     ### What do you want to see?? ==> behavior ASNs, from total, grouped by 100's changes a cay
#     ### we take the following dataframes:
#     # df_autnum (src3)  : contain count of autnum changes
#     # df_route (src4)   : contain count of route/6 changes
#     # df_total          : contain count of route/6 AND autnum changes combined - source: whois historical data api
#     # df_policy_total   : contain count of route/6 AND autnum changes combined - source: policy prefixes.
#     # print(src3,src3.keys())
#     src1_count = src1.reset_index().groupby([src1.reset_index().keys()[0]]).count()
#     print(src1_count)   
#     # src1_top5_count = src1_count.nlargest(5,"timestamp")
#     # print("grouby",src3_top5_count)
#     src1_grouped_count = src1_count.reset_index().round(-2).groupby("timestamp").count()
#     print(src1_grouped_count)
#     # print("grouby rounded",src3_grouped_count)
#     sns.set_style("darkgrid")
#     pal = sns.color_palette("Greens_d", len(src1_grouped_count))    
#     rank = src1_grouped_count[src1_grouped_count.reset_index().keys()[1]].argsort().argsort() 
#     ax = sns.barplot(x=src1_grouped_count.reset_index().keys()[0],\
#         y=src1_grouped_count.reset_index().keys()[4], \
#         data=src1_grouped_count.reset_index().nlargest(5,"aggregated_result"),\
#         palette=np.array(pal[::-1])[rank])
#     show_values_on_bars(ax)
#     # ax.set_xticklabels(ax.get_xticklabels(),rotation=30)
#     ax.set_xticklabels(["<100","<200","<300","<400",">400"],rotation=30)
#     plt.show()
def draw_bar_count_changes(src3,start='2019-01',end='2019-05'):
    src3_count = src3.reset_index().groupby([src3.reset_index().keys()[1]]).count()
    src3_grouped_count = src3_count.reset_index().round(-1).groupby("timestamp").count()
    sns.set_style("darkgrid")
    ax = sns.barplot(x=src3_grouped_count.reset_index().keys()[0], \
        y=src3_grouped_count.reset_index().keys()[1], data=src3_grouped_count.reset_index().nlargest(500,"asn"))
    ax.set_xticklabels(ax.get_xticks(),rotation=30)
    
    plt.show()

def draw_histogram_count_changes(src3,start='2001-01',end='2019-06'):
    ### we take the following dataframes:
    # df_autnum (src3)  : contain count of autnum changes
    # df_route (src4)   : contain count of route/6 changes
    # df_total          : contain count of route/6 AND autnum changes combined - source: whois historical data api
    # df_policy_total   : contain count of route/6 AND autnum changes combined - source: policy prefixes.

    src3_count = src3.loc[start:end].reset_index().groupby([src3.keys()[0]]).count()
    hist = src3_count.hist(bins=100)
    print("group by rounded", hist)
    sns.set_style("darkgrid")
    # src3_count.plot.hist(bins=5)
    hist=src3_count[['aggregated_result']]
    # ax=sns.distplot(hist)
    # plt.show()
    fig1,ax = plt.subplots()
    ax=sns.distplot(hist,kde=False)
    ax.set(xlabel='Changes per hour', ylabel='Number of changes per hour in each group')
    plt.title('Frequency of changes per hour')
    plt.show()

def draw_pie_count_changes(src3,start='2001-01',end='2019-06'):
    # print(src3,src3.keys())
    src3_count = src3.loc[start:end].reset_index().groupby([src3.reset_index().keys()[1]]).count()
    src3_grouped_count = src3_count.reset_index().round(-1).groupby("timestamp").count()
    sns.set_style("darkgrid")
    pal = sns.color_palette("Greens_d", len(src3_grouped_count))
    rank = src3_grouped_count[src3_grouped_count.reset_index().keys()[1]].argsort().argsort() 
    labels = "[0 and 100]","[101 and 200]","[201 and 300]","[301 and 400]","400 or more"
    color_palette_list = ['#009ACD', '#ADD8E6', '#63D1F4', '#0EBFE9',   
                      '#C1F0F6', '#0099CC']
    fig1,ax = plt.subplots()
    ax.pie(src3_grouped_count.reset_index().nlargest(5,"asn")[src3_grouped_count.reset_index().keys()[1]],\
        labels=labels, colors=color_palette_list[0:5],autopct='%1.1f%%', shadow=False)
    ax.axis('equal')
    ax.legend(frameon=False, bbox_to_anchor=(1.5,0.8))

    plt.show()

def draw_ts_period(src1,no_outliers=True,start='2001-01',end='2019-06'):
    sns.set_style("darkgrid")
    # sns.set_title(title)
    aggregated_result = src1.loc[start:end][['aggregated_result']]
    if no_outliers:
        aggregated_result_o=aggregated_result[(np.abs(stats.zscore(aggregated_result)) < 3).all(axis=1)]
        aggregated_result_o.columns=['Hourly samples']
        # print("this is it: ",aggregated_result_o.columns)
        aggregated_result_o_mean = aggregated_result_o['Hourly samples'].resample("M").mean()
        # ax=sns.lineplot(aggregated_result_o.loc[start:end],
        # marker='.', linestyle='-', linewidth=0.5, label='Hourly')
        ax=sns.lineplot(data=aggregated_result_o.loc[start:end])
        ax=sns.lineplot(data=aggregated_result_o_mean.loc[start:end],label='Monthly average')        
        # ax.set_xticklabels(ax.get_xticks(),rotation=30)
        ax.set(xlabel='Count of changes', ylabel='Time')
        ax.legend(framealpha=1, frameon=True)
    else:
        aggregated_result.columns=['Hourly samples']
        aggregated_result_mean = aggregated_result['Hourly samples'].resample("M").mean()
        ax=sns.lineplot(data=aggregated_result.loc[start:end])
        ax=sns.lineplot(data=aggregated_result_mean.loc[start:end],label="Monthly average")   
        # ax=plt.plot(aggregated_result.loc[start:end],
        # marker='.', linestyle='-', linewidth=0.5, label='Hourly')
        # ax=plt.plot(aggregated_result_mean.loc[start:end],
        # marker='o', markersize=8, linestyle='-', label="Monthly average")        
        # ax.set_xticklabels(ax.get_xticks,rotation=30)
    ax.set(xlabel='Time', ylabel='Count of changes')
    plt.title('Count of aut-num and route/route6 changes per hour')
    plt.show()

def draw_boxplot_source(src1,no_outliers=True,start='2001-01',end='2019-06'):
    sns.set_style("darkgrid")
    aggregated_result = src1.loc[start:end][['aggregated_result']]

    if no_outliers:
        aggregated_result_o=aggregated_result[(np.abs(stats.zscore(aggregated_result)) < 3).all(axis=1)]
        ax=sns.boxplot(x=aggregated_result_o)
    else:
        ax=sns.boxplot(x=aggregated_result)

    # ax=sns.boxplot(x=src1['aggregated_result'])
    # print(aggregated_result_o.shape,src1.shape)
    ax.set(xlabel='Count of changes')
    plt.show()

def draw_ts_rolling(src1,no_outliers=True,start='2001-01',end='2019-06'):
    sns.set_style("darkgrid")
    ## removing outliers:
    aggregated_result = src1.loc[start:end][['aggregated_result']]
    if no_outliers:
        aggregated_result_o=aggregated_result[(np.abs(stats.zscore(aggregated_result)) < 3).all(axis=1)]
        ax=aggregated_result_o.rolling(12).mean().plot(figsize=(20,10), linewidth=5, fontsize=20)
    else:
        ax=aggregated_result.rolling(12).mean().plot(figsize=(20,10), linewidth=5, fontsize=20)
    plt.xlabel('Changes per hour', fontsize=20)
    plt.show()

def draw_ts_diff(src1,no_outliers=True,start='2001-01',end='2019-06'):
    sns.set_style("darkgrid")
    ## removing outliers:
    aggregated_result = src1.loc[start:end][['aggregated_result']]
    if no_outliers:
        aggregated_result_o=aggregated_result[(np.abs(stats.zscore(aggregated_result)) < 3).all(axis=1)]
        ax=aggregated_result_o.diff().plot(figsize=(20,10), linewidth=5, fontsize=20)
    else:
        ax=aggregated_result.diff().plot(figsize=(20,10), linewidth=5, fontsize=20)
    plt.xlabel('Changes per hour', fontsize=20)
    plt.show()

def draw_ts_regplot(src1,no_outliers=True,start='2001-01',end='2019-06'):
    sns.set_style("darkgrid")
    ## removing outliers:
    aggregated_result = src1.loc[start:end][['aggregated_result']]
    if no_outliers:
        aggregated_result_o=aggregated_result[(np.abs(stats.zscore(aggregated_result)) < 3).all(axis=1)]
        aggregated_result_o_diff = aggregated_result_o.diff()
        ax=sns.regplot(x=aggregated_result_o_diff.reset_index().keys()[1],y=aggregated_result_o_diff.reset_index().keys()[0],data=aggregated_result_o_diff)
    else:
        aggregated_result_diff = aggregated_result.diff()
        ax=sns.regplot(x=aggregated_result_diff.reset_index().keys()[0],y=aggregated_result_diff.reset_index().keys()[1],data=aggregated_result_o_diff)
        # ax=aggregated_result.diff().plot(figsize=(20,10), linewidth=5, fontsize=20)
    plt.xlabel('Changes per hour', fontsize=20)
    plt.show()
def show_values_on_bars(axs):
    def _show_on_single_plot(ax):        
        for p in ax.patches:
            _x = p.get_x() + p.get_width() / 2
            _y = p.get_y() + p.get_height()
            value = '{:.2f}'.format(p.get_height())
            ax.text(_x, _y, value, ha="center") 

    if isinstance(axs, np.ndarray):
        for idx, ax in np.ndenumerate(axs):
            _show_on_single_plot(ax)
    else:
        _show_on_single_plot(axs)

