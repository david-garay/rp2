# !/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import os
from joblib import Parallel, delayed
import joblib
import sys
import time
import random

OUTPUT_FILE = "./results/prefixes_r2_2206.txt"
INPUT_FILE = "input/combined.txt"

def get_historical_data(line,new_format = False):
    if new_format:
        if "?" in line:
            as_number  = line.split("[")[1].split("?]")[0]
        elif "i" in line:    
            as_number  = line.split("[")[1].split("i]")[0]
        prefix =line.split("[")[0].strip()
    else:
        prefix = line.split(",")[2].split("|")[0].strip()
        as_number = line.split(",")[0]
    found = False
    print("Working with: %s"%(prefix))
    prefix_f = open(OUTPUT_FILE, "a")
    if os.path.isfile(OUTPUT_FILE):
        try:
            for line in prefix_f:
                #print(line,prefix)
                if prefix in line: 
                    found = True
                    print("Prefix found! ",prefix)
                    break
        except Exception as e:
            print("this one failed, ",e)    
                
    if found is False:
        print("Not in file!")
        try:    
            # time.sleep(0.1)
            r = requests.get('https://stat.ripe.net/data/historical-whois/data.json?resource='+prefix+'&sourceapp=os3_uva-sne-research')
            # out = open("logfile_route", "a")
            # out.write("%s,%s\n"%(prefix,r.json()))
            for version in r.json()['data']['versions']:
                # print(version)
                prefix_f.write("%s,%s,%s\n"%(as_number,prefix,version['from_time']))
        except Exception as e:
            print("shit! This one went wrong:", prefix, e)

def main():
    # print command line arguments
    if len(sys.argv) <= 1:
        try:
            with open(INPUT_FILE, 'r') as f:
                Parallel(n_jobs=joblib.cpu_count())(delayed(get_historical_data)(array,True) for array in f)

        except Exception as e:
                print("Something went wrong!",e)
    elif sys.argv[1]=="--prefixes":
        try:
            with open("./input/extended_policy_rs_2006.out", 'r') as f:
                Parallel(n_jobs=joblib.cpu_count())(delayed(get_historical_data)(line) for line in f)
                #for line in f: populate_members(line) 
        except IOError:
                print("Error opening input file")

if __name__ == '__main__':
    main()
