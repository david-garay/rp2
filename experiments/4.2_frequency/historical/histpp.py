# !/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import os
from joblib import Parallel, delayed
import joblib
import subprocess
import time

def populate_file(line):
    # asn,obj_type,prefix= line.split(",")
    time.sleep(1)
    asn= line.strip()
    # asn="AS1103"
    print("Working with: %s"%(asn))
    # if not os.path.isfile("./results/autnum_"+(as_number.strip())):
    route_f = open("./results/policies_800.txt", "a")
    # f_autnum    = line.split("^")[0]
    f_import    = whois(asn)
    # f_export    = whois(asn,'/export:/,/notify:/p')
    # f_as_set    = whois(asn,'/as-set:/,/descr:/p')
    # f_route_set = whois(asn,'/route-set:/,/descr:/p')
    # f_members   = whois(asn,'/members:/,/descr:/p')
    # route_f.write("%s,%s,%s,%s,%s,%s\n"%(asn,f_import,\
    #         f_export,f_as_set,f_route_set,f_members))    
    route_f.write("%s/n %s\n"%(asn,f_import))
    
def whois(asn,grep_str=''):
    # p1 = subprocess.Popen(["whois", asn], stdout=subprocess.PIPE)
    # p2 = subprocess.check_output(["grep", grep_str], stdin=p1.stdout)
    # p1.wait()
    # # p1.stdout.close()
    # return p2.stdout.read()
    if grep_str:
        cmd = "whois " + asn + " |sed -n '" + grep_str +"'|grep -v '" + grep_str.split("/")[3] + "'"+ "|paste -s -d\"&&\""
    else:
        cmd = "whois " + asn
    print(cmd)
    ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    return output.strip()
if __name__ == '__main__':
    try:
        with open("./as_list.txt", 'r') as f:
            # geoprocessing stuff
            # for line in f:
            #     asn,obj_type,prefix = line.split(",")
            Parallel(n_jobs=joblib.cpu_count())(delayed(populate_file)(line) for line in f)
                
    except IOError:
            print("Error opening as_list.txt")
