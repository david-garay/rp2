# !/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import os
from joblib import Parallel, delayed
import joblib
import sys
import time
import random

def get_historical_data(as_number):
    print("Working with: %s"%(as_number.strip()))
    # if not os.path.isfile("./results/autnum_"+(as_number.strip())):
    time.sleep(random.randint(0,3))

    r = requests.get('https://stat.ripe.net/data/historical-whois/data.json?resource='+as_number+'&sourceapp=os3_uva-sne-research')
    autnum_f = open("./results/autnum_800_rs_2206.txt", "a")
    autnum_f.write("as_number,change_history\n")
    for version in r.json()['data']['versions']:
        # print(version)
        autnum_f.write("%s,%s\n"%(as_number,version['from_time']))
    #
    route_f = open("./results/route_800.txt", "a")
    route_f.write("as_number,referenced_by\n")
    for route in r.json()['data']['referenced_by']:
        # print(route)
        ### RELEVANT OR NOT?
        # relevant = is_relevant(as_number, route[0]['key'])
        route_f.write("%s,%s,%s\n"%(as_number,route[0]['type'],route[0]['key']))
    #
    # log_f = open("logfile", "a"),
    # log_f.write("%s,%s\n"%(as_number,r.json()))

# def is_relevant(as_number, prefix):
#         ## read policy file
#         # If prefix excluded in import OR export policy, for either AS-Sets or Route-sets (), irrelevant
#         relevant = True
#         try:
#             policies_f = open("./policies_abc.txt", "a")
#             for as_policy_line in policies_f:
#                 asn, import_policy, export_policy  = as_policy_line.split(",")
#                 if as_number == asn:
#   #  Check import policies
#                     if 'ANY AND NOT' in import_policy:
#                         if '<^[' in import_policy:
#                             excluded_asns = import_policy.split("ANY AND NOT")[1].split('<^[')[1].split("]>")[0]
#                             for excluded_asn in excluded_asns:
#                                 if as_number in excluded_asn:
#                                     relevant = False
#                                         break
#                         elif '{' in import_policy:
#                             if '<^[' in import_policy:
#                                 excluded_prefixes = import_policy.split("ANY AND NOT")[1].split('{')[1].split("}")[0]
#                                 for excluded_prefix in excluded_prefixes:
#                                     if prefix in excluded_prefix:
#                                         relevant = False
#                                         break
#                         else:
#                             # here we might have asns or as-set names
#                             # if '-' in import_policy
#                             as_sets = import_policy.split("ANY AND NOT")[1].split()
#                             # are there more than 1? can it be an as?
#                             for as_set in as_sets:
#                                 if "-" in as_set:
#                                     if as_number in as_set_members:
#                                         relevant = False
#                                         break
#                                 else:
#                                     if as_number == as_set:
#                                         relevant = False
#                                         break
#                             # read as-set members
#                     elif 'ANY':  

#                     else:
                
#                 break
#                         # read as-set members
#   #  Check export policies
#                     if 'ANY AND NOT' in export_policy:
#                         if '<^[' in export_policy:
#                         elif '{' in export_policy:
#                         else:
#                             # read as-set members
#                     elif 'ANY':  

#                     else:
#                         # read as-set members

#                     break 

#         except Exception as e:
#             print("is_relevant, AS-set went wrong: ",e)
#         return relevant

def main():
    # print command line arguments
    sys.setrecursionlimit(30000)
    if len(sys.argv) <= 1:
        try:
            with open("./input/as_list_rs.txt", 'r') as f:
                # geoprocessing stuff
                for array in f:
                    as_array = array.split(",")
                    Parallel(n_jobs=joblib.cpu_count())(delayed(get_historical_data)(as_number.strip()) for as_number in as_array)
                    
        except IOError:
                print("Error opening as_list.txt")
    elif sys.argv[1]=="--prefixes":
        try:
            with open("./results/extended_policy_rs_2006.out", 'r') as f:
                Parallel(n_jobs=joblib.cpu_count())(delayed(get_historical_data)(line) for line in f)
                #for line in f: populate_members(line) 
        except IOError:
                print("Error opening input file")

if __name__ == '__main__':
    main()