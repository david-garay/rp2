# !/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import os
from joblib import Parallel, delayed
import joblib
import sys
import time
import random

def generate_client(as_number,prefix):
    print("Working with: %s"%(as_number.strip()))

    autnum_f = open("./results/brdcfg_"+as_number+".txt", "a",newline='\n')
    autnum_f.write("router id "+prefix+";\n\n")
    autnum_f.write("protocol direct {\n")
    autnum_f.write("\tinterface \"lo\";\n")
    autnum_f.write("}\n\n")
    autnum_f.write("protocol kernel {\n")
    autnum_f.write("\tpersist;\n")
    autnum_f.write("\tscan time 20;\n")
    autnum_f.write("\timport all;\n")
    autnum_f.write("\texport all;\n")
    autnum_f.write("}\n\n")
    autnum_f.write("protocol device {\n")
    autnum_f.write("\tscan time 10;\n")
    autnum_f.write("}\n\n")
    autnum_f.write("protocol direct {\n")
    autnum_f.write("\tinterface \"eth*\";\n")
    autnum_f.write("}\n\n")
    autnum_f.write("protocol bgp {\n")
    autnum_f.write("\tlocal as "+as_number.split("AS")[1]+";\n")
    autnum_f.write("\tneighbor 192.168.0.3 as 65001;\n")
    autnum_f.write("\texport all;\n")
    autnum_f.write("\timport all;\n")
    autnum_f.write("}\n\n")

def generate_rs(as_number,prefix,content):
    print("Working with: %s"%(as_number))
    keyword="as "+as_number.strip("AS")
    keyword_2="_prefixes = ["
    add_prefix = False
    for index, line in enumerate(content,start=0):
        if keyword in line:
            if "neighbor" in line:
                content[index]="    neighbor "+prefix+" "+keyword+";"
        elif keyword_2 in line:
            if line.split("AS_SET_")[1].split("_prefixes")[0] == as_number:
                if "];" not in content[index+1]:
                    add_prefix=True
        else:
            if add_prefix:
                content[index]="37.10.32.0/31{24,32},\n"
                add_prefix=False
    return content

def main():
    # print command line arguments
    sys.setrecursionlimit(30000)
    if len(sys.argv) <= 1:
        try:
            with open("./resources/asn_prefixes.txt", 'r') as f:
                for array in f:
                    as_number = array.split(",")[0].strip()
                    prefix = array.split(",")[1].strip()
                    generate_client(as_number, prefix)
                    
        except Exception as e:
                print("Error: ",e)
    elif sys.argv[1]=="--rs":
        try:
            # with open("./resources/big_bird_tst.txt", 'r') as f:
            with open("./resources/corrected_AMS_bird.txt", 'r') as f:
                content = f.readlines()
                print(len(content))
            # with open("./resources/asn_prefixes_tst.txt", 'r') as f:
            with open("./resources/asn_prefixes.txt", 'r') as f:
                for array in f:
                    as_number = array.split(",")[0].strip()
                    prefix = array.split(",")[1].strip()
                    content=generate_rs(as_number, prefix,content)
            output_f = open("./results/corrected_AMS_bird_output_v1.txt", "a",newline='\n')
            output_f.write("".join(content))
        except Exception as e:
                print("Error: ",e)

if __name__ == '__main__':
    main()