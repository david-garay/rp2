# !/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import os
from joblib import Parallel, delayed
import joblib
import subprocess
import time
import sys
import random

def populate_file(line):
    asn= line.strip()
    time.sleep(random.randint(0,5))
    print("Working with: %s"%(asn))
    route_f = open("./results/whois_844_6777_2006.txt", "a")
    f_import    = whois(asn)
    f_export    = whois(asn,"export")
    route_f.write("%s,%s,%s\n"%(asn,f_import,f_export))
    
def whois(asn,grep_typ='import'):
    if grep_typ == 'import':
        cmd = "whois " + asn +  "| grep -E -A 4 'from AS6777'|grep -v 'import-via'|grep -v remarks "
    else:
        cmd = "whois " + asn +  "| grep -E -A 4 'to AS6777'|grep -v 'export-via'|grep -v remarks "
    ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
    output = ps.communicate()[0]
    out = ''

    try:
        if grep_typ == 'import' and output:
            out = str(output).split('import:')[1].split("accept")[1].split("\\")[0].split("\n")[0].strip()
        elif output:
            out = str(output).split('export:')[1].split("announce")[1].split("\\")[0].split("\n")[0].strip()
    except Exception as e:
        print ("whois failed: ",e,output,asn,grep_typ)
    return out.strip("\n")

# AS1103, ANY, AS-SURFNET
def populate_members(line):
    asn         = line.split(",")[0].strip()
    f_import    = line.split(",")[1].strip()
    f_export    = line.split(",")[2].strip()
    time.sleep(1)
    print("Working with: %s"%(asn))
    route_f = open("./results/extended_policy_rs_2006.out", "a")
    # if not(f_export and f_import):
    #     f_export_members = ""
    #     f_import_members = ""
    # else:    
    f_export_members = whois_asset(asn,f_export,"export")
    f_import_members = whois_asset(asn,f_import)

    route_f.write("%s,%s,%s,%s,%s\n"%(asn,f_import,f_export,f_import_members,f_export_members))

def whois_asset(asn,pol_line,grep_typ='import'):
    cmd = ''
    if pol_line:
        try:    
            if grep_typ == 'import':
                if ('ANY' or 'any') not in pol_line:
                    for pol in pol_line.split():
                        if "-" in pol:
                            #cmd = "whois  -r -T as-set " + pol + " | grep members: |  paste -sd ="
                            cmd = "bgpq3 -b " + pol + " | paste -sd ="
                        elif "AS" in pol:
                            cmd = "bgpq3 -b " + asn + " | paste -sd ="
                else:
                    return   
            else:
                for pol in pol_line.split():
                    if "-" in pol:
                        #cmd = "whois  -r -T as-set " + pol + " | grep members: |  paste -sd ="     
                        cmd = "bgpq3 -b " + pol + " | paste -sd ="
                    elif "AS" in pol:
                        cmd = "bgpq3 -b " + asn + " | paste -sd ="
            ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
            output = ps.communicate()[0]
            members = str(output).split('NN = [')[1]
        except Exception as e:
            print("something wrong: ",e,output,asn,pol_line,grep_typ)
            return
    else:
        try:    
            cmd = "bgpq3 -b " + asn + " | paste -sd ="
            ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
            output = ps.communicate()[0]
            members = str(output).split('NN = [')[1]
        except Exception as e:
            print("something wrong: ",e,output,asn,pol_line)
            return

    result= []
#    for member in members:
#        member=member.strip("=").strip()
#       if member.strip(",")=='': continue
#        elif "-" in member.strip(","):
#            result=whois_asset(asn,member)
#        else:
#            result.append(member)
    for member in members.split(","):
        result.append(member.strip("=").strip().strip("=];"))
    return '^'.join(result)

def main():
    # print command line arguments
    sys.setrecursionlimit(30000)
    if len(sys.argv) <= 1:
        try:
            with open("./as_list_rs.txt", 'r') as f:
                Parallel(n_jobs=joblib.cpu_count())(delayed(populate_file)(line) for line in f)
                    
        except IOError:
                print("Error opening as_list.txt")
    elif sys.argv[1]=="--populate_members":
        try:
            with open("./results/whois_844_6777_2006.txt", 'r') as f:
                Parallel(n_jobs=joblib.cpu_count())(delayed(populate_members)(line) for line in f)
                #for line in f: populate_members(line) 
        except IOError:
                print("Error opening as_list.txt")

if __name__ == '__main__':
    main()
